import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
class CircularReusableWidget extends StatelessWidget {

  final VoidCallback onclick;
  final bgcolor;

  //ReusableWidget({required this.onPress,required this.bgcolor,required this.columnwidget});
  CircularReusableWidget({required this.onclick,this.bgcolor});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onclick,
      child: Container(
        width: 40,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: bgcolor
        ),
      ),
    );
  }
}
