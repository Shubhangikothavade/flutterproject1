import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:project1_flutter/Constants/AllConstants.dart';
class ReusableWidget extends StatelessWidget {
  final VoidCallback onPress;
  //final Color bgcolor;
  final image;
  final title;
  final subtitle;

  //ReusableWidget({required this.onPress,required this.bgcolor,required this.columnwidget});
  ReusableWidget({required this.onPress,this.image,this.title,this.subtitle});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        margin: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SvgPicture.asset('asset/$image'),
              Text('$title',
              style: kAllRoomsText,),
              Text('$subtitle',
                  style: kSubTitle,),
            ],
          ),
        ),
      ),
    );
  }
}