import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
class SceneReusableWidget extends StatelessWidget {

  final title;
  final bgcolor;

  //ReusableWidget({required this.onPress,required this.bgcolor,required this.columnwidget});
  SceneReusableWidget({this.title,this.bgcolor});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: 200,
      margin: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20.0),
        color: bgcolor,
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SvgPicture.asset('asset/surface2.svg'),
            Text('$title',
              style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold
              ),),

          ],
        ),
      ),
    );
  }
}