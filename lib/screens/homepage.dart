import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:project1_flutter/Constants/AllConstants.dart';
import 'package:project1_flutter/classes/reusablewidget.dart';

import 'detailpage.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[900],
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: ListTile(
              title: Text(
                'Control',
                style: TextStyle(
                    fontSize: 50,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                'Panel',
                style: kSubHeading.copyWith(
                  color: Colors.white
                ),
              ),
              trailing: CircleAvatar(
                child: Icon(
                    Icons.person,
                ),
                radius: 30,
              ),
            ),
          ),
          Container(
            height: 700,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(50),
                    topLeft: Radius.circular(50)),
                color: Colors.white54),
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(children: [
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'All Rooms',
                    style: kSubHeading.copyWith(
                      color: Colors.blue[900]
                    ),
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: ReusableWidget(
                          image: 'bed.svg',
                          title: 'Bed room',
                          subtitle: '4 Lights',
                          onPress: () {
                            setState(() {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      DetailPage(),
                                ),
                              );
                            });
                          },
                        ),
                      ),
                      Expanded(
                        child: ReusableWidget(
                          image: 'room.svg',
                          title: 'Living room',
                          subtitle: '2 lights',
                          onPress: () {},
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: ReusableWidget(
                          image: 'kitchen.svg',
                          title: 'Kitchen',
                          subtitle: '5 Lights',
                          onPress: () {},
                        ),
                      ),
                      Expanded(
                        child: ReusableWidget(
                          onPress: () {},
                          image: 'bathtube.svg',
                          title: 'Bathroom',
                          subtitle: '1 lights',
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: ReusableWidget(
                          image: 'house.svg',
                          title: 'Outdoor',
                          subtitle: '5 lights',
                          onPress: () {},
                        ),
                      ),
                      Expanded(
                        child: ReusableWidget(
                          onPress: () {},
                          image: 'balcony.svg',
                          title: 'Balcony',
                          subtitle: '2 lights',
                        ),
                      ),
                    ],
                  ),
                ),
              ]),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        clipBehavior: Clip.antiAlias,
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 50.0,
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MaterialButton(
                onPressed: () {
                  setState(() {
                    _currentIndex = 1;
                  });
                },
                child: SvgPicture.asset('asset/bulb.svg',
                  width: 30,),
              ),
              MaterialButton(
                onPressed: () {
                  setState(() {
                    _currentIndex = 2;
                  });
                },
                child: SvgPicture.asset(
                  'asset/Icon feather-home.svg',
                  width: 40,
                ),
              ),
              MaterialButton(
                onPressed: () {
                  setState(() {
                    _currentIndex = 3;
                  });
                },
                child: SvgPicture.asset(
                  'asset/Icon feather-settings.svg',
                  width: 40,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
