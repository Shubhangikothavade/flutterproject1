import 'package:flutter/material.dart';

class TestClass extends StatefulWidget {
  @override
  _TestClassState createState() => _TestClassState();
}

class _TestClassState extends State<TestClass> with SingleTickerProviderStateMixin{
  late AnimationController _animationController;
  late Animation _tweenValue;

  void initState() {
    // TODO: implement initState
    super.initState();
    _animationController =
        AnimationController(duration: Duration(seconds: 2), vsync: this);
    _tweenValue = Tween(begin: 0, end: 1)
        .animate(_animationController);
    _animationController.forward();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('test'),),
      body: Center(
          child: Container(
              height: 250,
              width: 250,
              color: Colors.blue,
            ),
        ),
        );
  }
}

/*
 AnimatedBuilder(
          animation: _animationController.view,
          builder: (context,child){
            return Transform.rotate(angle: 20);
          },
          child:
 */