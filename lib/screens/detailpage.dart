import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:project1_flutter/Constants/AllConstants.dart';
import 'package:project1_flutter/classes/circularreusblewidget.dart';
import 'package:project1_flutter/classes/reusablewidget.dart';
import 'package:project1_flutter/classes/scenereusablewidget.dart';

import 'package:animations/animations.dart';

class DetailPage extends StatefulWidget {
  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage>
    with SingleTickerProviderStateMixin {
  int _currentIndex = 0;
  MainAxisAlignment varmain = MainAxisAlignment.spaceEvenly;
  int _selectedIndex = 0;
  double varChangedval = 120;
  Color _bulbColor = Colors.pink.shade200;

  late AnimationController _animationController;
  late Animation<Offset> _tweenValue;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _animationController =
        AnimationController(duration: Duration(seconds: 1), vsync: this);
    _tweenValue = Tween<Offset>(begin: Offset(10, 0), end: Offset(0, 0))
        .animate(_animationController);
    _animationController.forward();

    // _animationControllerForColorAxis = AnimationController(vsync: this,
    // duration: Duration(seconds: 2));
    // _tweenValueForColorAxis = Tween<MainAxisAlignment>(begin:MainAxisAlignment.start)
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[900],
      body: SafeArea(
        child: ListView(
          children: [
            Container(
              padding: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          GestureDetector(
                            child: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                              size: 50,
                            ),
                            onTap: () {
                              Navigator.pop(context);
                            },
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Bed',
                            style: kSubHeading,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text('Room', style: kSubHeading),
                      SizedBox(
                        height: 5,
                      ),
                      Text('4 Lights',
                          style:
                              kSubHeading.copyWith(color: Colors.orangeAccent))
                    ],
                  ),
                  Stack(
                    clipBehavior: Clip.none,
                    children: [
                      Positioned(
                        left: 50,
                        top: 110,
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: _bulbColor,
                          ),
                        ),
                      ),
                      SvgPicture.asset('asset/Group 38.svg'),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            SlideTransition(
              position: _tweenValue,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                          height: 50,
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)),
                              color: Colors.white),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              SvgPicture.asset(
                                'asset/surface1.svg',
                                width: 20,
                              ),
                              Text(
                                'Main Light',
                                style: kSlideWidget,
                              )
                            ],
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                          height: 50,
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)),
                              color: Colors.orangeAccent),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              SvgPicture.asset(
                                'asset/furniture-and-household.svg',
                                width: 20,
                              ),
                              Text('Desk Lights', style: kSlideWidget)
                            ],
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                          height: 50,
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)),
                              color: Colors.white),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              SvgPicture.asset(
                                'asset/bed (1).svg',
                                width: 20,
                              ),
                              Text('Bed', style: kSlideWidget)
                            ],
                          )),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Stack(clipBehavior: Clip.none, children: [
              Positioned(
                  right: 30,
                  top: -20,
                  child: SvgPicture.asset(
                    'asset/Icon awesome-power-off.svg',
                    width: 40,
                  )),
              Container(
                height: 500,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(50),
                        topLeft: Radius.circular(50)),
                    color: Colors.white54),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(children: [
                    Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Intensity',
                          style: kSubHeading.copyWith(color: Colors.blue[900]),
                        )),
                    Expanded(
                      child: SliderTheme(
                        data: SliderTheme.of(context).copyWith(
                          activeTrackColor: Colors.amberAccent,
                          inactiveTrackColor: Colors.black38,
                          overlayColor: Colors.amberAccent,
                          thumbColor: Colors.white,
                        ),
                        child: Slider(
                          value: varChangedval,
                          //value: 120,
                          min: 100.0,
                          max: 200.0,
                          onChanged: (value) {
                            setState(() {
                              varChangedval = value;
                            });
                          },
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text('Colors',
                          style: kSubHeading.copyWith(
                            color: Colors.blue[900],
                          )),
                    ),
                    Expanded(
                      child: Row(
                        mainAxisAlignment: varmain,
                        children: [
                          CircularReusableWidget(
                            onclick: () {
                              setState(() {
                                _bulbColor = Colors.pink.shade200;
                              });
                            },
                            bgcolor: Colors.pink.shade200,
                          ),
                          CircularReusableWidget(
                            onclick: () {
                              setState(() {
                                _bulbColor = Colors.green.shade200;
                              });
                            },
                            bgcolor: Colors.green.shade200,
                          ),
                          CircularReusableWidget(
                            onclick: () {
                              setState(() {
                                _bulbColor = Colors.blue.shade200;
                              });
                            },
                            bgcolor: Colors.blue[200],
                          ),
                          CircularReusableWidget(
                            onclick: () {
                              setState(() {
                                _bulbColor = Colors.purpleAccent.shade200;
                              });
                            },
                            bgcolor: Colors.purpleAccent[200],
                          ),
                          CircularReusableWidget(
                            onclick: () {
                              setState(() {
                                _bulbColor = Colors.orangeAccent.shade200;
                              });
                            },
                            bgcolor: Colors.orangeAccent[200],
                          ),
                          CircularReusableWidget(
                            onclick: () {
                              setState(() {
                                _bulbColor = Colors.brown.shade200;
                              });
                            },
                            bgcolor: Colors.brown[200],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text('Scenes',
                          style: kSubHeading.copyWith(
                            color: Colors.blue[900],
                          )),
                    ),
                    Expanded(
                      child: Row(
                        children: [
                          Expanded(
                            child: SceneReusableWidget(
                              title: 'Birthday',
                              bgcolor: Colors.pinkAccent,
                            ),
                          ),
                          Expanded(
                            child: SceneReusableWidget(
                              title: 'Party',
                              bgcolor: Colors.purpleAccent,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Row(
                        children: [
                          Expanded(
                            child: SceneReusableWidget(
                              title: 'Relax',
                              bgcolor: Colors.lightBlue,
                            ),
                          ),
                          Expanded(
                            child: SceneReusableWidget(
                              title: 'Fun',
                              bgcolor: Colors.lightGreen,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
            ]),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        clipBehavior: Clip.antiAlias,
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 50.0,
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MaterialButton(
                onPressed: () {
                  setState(() {
                    _currentIndex = 1;
                  });
                },
                child: SvgPicture.asset('asset/bulb.svg',
                width: 30,),
              ),
              MaterialButton(
                onPressed: () {
                  setState(() {
                    _currentIndex = 2;
                  });
                },
                child: SvgPicture.asset(
                  'asset/Icon feather-home.svg',
                  width: 40,
                ),
              ),
              MaterialButton(
                onPressed: () {
                  setState(() {
                    _currentIndex = 3;
                  });
                },
                child: SvgPicture.asset(
                  'asset/Icon feather-settings.svg',
                  width: 40,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _animationController.dispose();
  }
}
