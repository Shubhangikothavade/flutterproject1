import 'package:flutter/material.dart';

var kAllRoomsText = TextStyle(
    fontSize: 25,
    fontWeight: FontWeight.bold
);

var kSubTitle = TextStyle(
    fontSize: 20,
    color: Colors.orangeAccent,
    fontWeight: FontWeight.bold
);

var kSubHeading = TextStyle(
    fontSize: 40,
    color: Colors.white,
    fontWeight: FontWeight.bold);

var kSlideWidget = TextStyle(
    fontSize: 20,
    color: Colors.blue[900],
    fontWeight: FontWeight.bold);